#include "terminalplayer.hpp"

std::string backgammon::detail::token(backgammon::Player p) {
    switch (p) {
    case Player::Second:
        return "B"; //"○";
    case Player::First:
        return "W"; //"●";
    default:
        return " ";
    }
}

void backgammon::detail::printGame(const backgammon::Board &field) {
    int h = 6;
    for (auto token: field) {
        h = std::max(token.tokens(), h);
    }
    std::cout << "to place: "
              << token(Player::First) << ' ' << field.placeable() << ", "
              << token(Player::Second) << ' ' << field.opponentView().placeable() << std::endl;
    for (size_t i = field.size() / 2; i < field.size(); ++i) {
        std::cout << std::setw(3) << (i + 1);
        if (i == 3 * field.size() / 4 - 1)
            std::cout << " |";
    }
    std::cout << std::endl;

    for (int y = 0; y < h; ++y) {
        for (size_t i = field.size() / 2; i < field.size(); ++i) {
            auto t = field[i];
            if (t.tokens() > y)
                std::cout << std::setw(3) << token(t.player());
            else
                std::cout << std::setw(3) << ' ';
            if (i == 3 * field.size() / 4 - 1)
                std::cout << " |";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int y = 0; y < h; ++y) {
        for (size_t i = field.size() / 2; i--;) {
            auto t = field[i];
            if (t.tokens() >= h - y)
                std::cout << std::setw(3) << token(t.player());
            else
                std::cout << std::setw(3) << ' ';
            if (i == field.size() / 4)
                std::cout << " |";
        }
        std::cout << std::endl;
    }

    for (size_t i = field.size() / 2; i--;) {
        std::cout << std::setw(3) << (i + 1);
        if (i == field.size() / 4)
            std::cout << " |";
    }
    std::cout << std::endl;
}

std::pair<std::vector<int>, backgammon::Board> backgammon::detail::terminalPlayerIntermediate(const backgammon::Board &b, const std::vector<int> &dice, std::vector<backgammon::Action> &actions) {
    std::string commandLine;
    bool done = false;
    auto newdice(dice);
    Board newboard = b;
    while (!done && std::cin) {
        std::getline(std::cin, commandLine);
        std::stringstream ss(commandLine);
        std::string cmd;
        ss >> cmd;
        if (cmd == "m") {
            size_t from, step;
            ss >> from >> step;
            if (!ss.fail()) {
                --from; // indexing with 1 instead
                auto it = std::find(dice.begin(), dice.end(), step);
                if (it != dice.end() && b.canMove(from, step)) {
                    actions.push_back(Move{from, step});
                    newdice.erase(newdice.begin() + (it - dice.begin()));
                    done = true;
                    return {newdice, b.moved(from, step)};
                } else {
                    std::cout << "invalid move, try again" << std::endl;
                }
            }
        } else if (cmd == "p") {
            size_t at;
            ss >> at;
            if (!ss.fail()) {
                --at; // indexing starts with 1
                auto it = std::find(dice.begin(), dice.end(), at);
                if (it != dice.end() && b.canInsert(at)) {
                    actions.push_back(Insert{at});
                    newdice.erase(newdice.begin() + (it - dice.begin()));
                    done = true;
                    return {newdice, b.inserted(at)};
                } else {
                    std::cout << "invalid move, try again" << std::endl;
                }
            }
        } else {
            std::cout << "invalid command " << cmd << std::endl;
        }
    }
    throw std::runtime_error("aborted");
}

void backgammon::detail::terminalPlayerStep(
        const backgammon::Board &b,
        const std::vector<int> &dice,
        std::vector<backgammon::Action> &actions)
{
    printGame(b);
    if (dice.empty()) {
        return;
    } else {
        std::cout << "Available dice: ";
        for (auto d: dice) {
            std::cout << d << ' ';
        }
        std::cout << std::endl;
    }
    if (b.placeable()) {
        std::cout << "token to be re-placed: " << b.placeable() << std::endl;
    }
    std::cout << "Turn: (m <> <> or p <>): ";
    auto [newdice, newboard] = terminalPlayerIntermediate(b, dice, actions);
    terminalPlayerStep(newboard, newdice, actions);
}

std::vector<backgammon::Action> backgammon::terminalPlayer(const backgammon::Board &b, const std::vector<int> &dice) {
    std::vector<Action> actions;
    detail::terminalPlayerStep(b, dice, actions);
    return actions;
}
