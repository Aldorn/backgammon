#include <iostream>

#include "backgammon.hpp"
#include "terminalplayer.hpp"


int main() {
    backgammon::Game::runGame(backgammon::terminalPlayer, backgammon::terminalPlayer);
    return 0;
}
