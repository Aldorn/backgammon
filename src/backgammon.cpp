#include "backgammon.hpp"

namespace backgammon
{
namespace  {
std::vector<int> DEFAULT_GAME = {-2, 0, 0, 0, 0, 5, 0, 3, 0, 0, 0, -5, 5, 0, 0, 0, -3, 0, -5, 0, 0, 0, 0, 2};
}

detail::Field::Field() :
    Field(DEFAULT_GAME) { }

detail::Field::Field(const std::vector<int> &initial)
    : m_tokens(initial) {  }

Board::Board(const detail::Field &f, const Player &player)
    : m_field(f), m_player(player) {}

void Board::move(const size_t from, const size_t steps) {
    if (!canMove(from, steps))
        throw std::runtime_error("forbidden move");
    if (m_player == Player::Second) {
        int& source = m_field.m_tokens[size() - from - 1];
        int& dest = m_field.m_tokens[size() - from - steps - 1];
        // Last move outside field?
        if (from + steps < size()) {
            if (dest > 0) {
                m_field.toPlace(Player::First) += std::abs(dest);
                dest = -1;
            }
            else
                --dest;
        }
        ++source;
    } else {
        int& source = m_field.m_tokens[from];
        int& dest = m_field.m_tokens[from + steps];
        // Last move outside field?
        if (from + steps < size()) {
            if (dest < 0) {
                m_field.toPlace(Player::Second) += std::abs(dest);
                dest = 1;
            }
            else
                ++dest;
        }
        --source;
    }
}

void Board::insert(size_t at) {
    if (!canInsert(at))
        throw std::runtime_error("forbidden move");
    if (m_player == Player::Second)
        at = size() - at - 1;
    if (m_player != Player::Second) {
        if (m_field.m_tokens[at] < 0) {
            m_field.m_tokens[at] = 1;
            ++m_field.m_placeable[Player::First];
        } else {
            ++m_field.m_tokens[at];
        }
    } else {
        if (m_field.m_tokens[at] > 0) {
            m_field.m_tokens[at] = -1;
            ++m_field.m_placeable[Player::Second];
        } else {
            --m_field.m_tokens[at];
        }
    }
}

Board::Board(const std::vector<int> &initial)
    : m_field(initial) {}



FieldState Board::operator[](size_t index) const { return *(begin() + index); }

Board::iterator Board::begin() const { return iterator(m_field, m_player); }

Board::iterator Board::end() const { return iterator(m_field, m_player) + size(); }

Board::reverse_iterator Board::rbegin() const { return reverse_iterator(m_field, m_player); }

Board::reverse_iterator Board::rend() const { return reverse_iterator(m_field, m_player) + size();}

Board Board::opponentView() const { return Board(m_field, m_player.opponent());}

Board Board::byPlayer(Player p) const { return Board(m_field, p);}

int Board::placeable() const { return m_field.toPlace(m_player); }

Board Board::moved(size_t from, size_t steps) const {
    if (!canMove(from, steps))
        throw std::runtime_error("forbidden move");
    Board result(*this);
    result.move(from, steps);
    return result;
}

Board Board::inserted(size_t at) const {
    if (!canInsert(at))
        throw std::runtime_error("forbidden move");
    Board result(*this);
    result.insert(at);
    return result;
}

bool Board::canMove(size_t from, size_t step) const {
    if (from < 0 || step < 0)
        return false;
    else if (placeable() > 0)
        return false;
    else if ((*this)[from].player() != Player::First || (*this)[from].tokens() == 0)
        return false;
    else
        return (from + step) >= size() // last move off the board
                || (*this)[from + step].player() != Player::Second
                || (*this)[from + step].tokens() == 1;
}

bool Board::canInsert(size_t at) const {
    if (at > size() / 4)
        return false;
    else if (!placeable())
        return false;
    else
        return (*this)[at].player() != Player::Second || (*this)[at].tokens() == 1;
}

void Game::operator()(const Move &m) {
    board = board.moved(m.from(), m.step());
}

void Game::operator()(const Insert &m) {
    board = board.inserted(m.at());
}

bool Game::check(const std::vector<Action> &actions, std::vector<int> dice) {
    std::vector<int> actionDice;
    for (auto a: actions)
        actionDice.push_back(std::visit(usedDie,a));
    std::sort(dice.begin(), dice.end());
    std::sort(actionDice.begin(), actionDice.end());
    if (!std::includes(dice.begin(), dice.end(), actionDice.begin(), actionDice.end())) {
        return false;
    }
    return true;
}

void Game::apply(const std::vector<Action> &actions) {
    board = applier(board, actions);
}

void Game::run() {
    bool firstPlayer = true;
    std::default_random_engine rng;
    std::uniform_int_distribution<> uid(1,6);
    while (true) {

        std::vector<int> dice;
        for (int i = 0; i < 2; ++i) {
            dice.push_back(uid(rng));
        }
        std::vector<Action> actions;
        if (firstPlayer) {
            std::cout << "player 1" << std::endl;
            actions = player1(board, dice);
        } else {
            std::cout << "player 2" << std::endl;
            actions = player2(board, dice);
        }
        if (!check(actions, dice)) {
            std::cout << "invalid dice, try again" << std::endl;
        } else {
            apply(actions);
            board = board.opponentView();
            firstPlayer = !firstPlayer;
        }
    }
}

void Game::runGame(PlayerFunction p1, PlayerFunction p2) {
    Game g(p1, p2, 0);
    g.run();
}

Board Game::Applier::operator()(const Move &m) {
    return b.moved(m.from(), m.step());
}

Board Game::Applier::operator()(const Insert &i) {
    return b.inserted(i.at());
}

Board Game::Applier::operator()(Board board, const std::vector<Action> &actions) {
    for (auto a: actions) {
        Applier app{board};
        board = std::visit(app, a);
    }
    return board;
}

int Game::DiceCounter::operator()(const Move &m){ return m.step(); }

int Game::DiceCounter::operator()(const Insert &i) { return i.at(); }

int Game::DiceCounter::operator()(const Action &a) { return std::visit(*this, a);}

Insert::Insert(size_t at)
    : m_at(at) {}

size_t Insert::at() const { return m_at;}

Move::Move(size_t from, size_t step)
    : m_from(from), m_step(step) {}

size_t Move::from() const { return m_from;}

size_t Move::step() const { return m_step;}







}// namespace backgammon
