#ifndef BACKGAMMON_HPP
#define BACKGAMMON_HPP

#include <vector>
#include <iostream>
#include <variant>
#include <memory>
#include <algorithm>
#include <random>

namespace backgammon {

class Player {
public:
    enum Value : uint8_t {
        First,
        Second,
        None
    };
    Player() = default;
    constexpr Player(Value p) : m_player(p) {}
    constexpr operator Value() const { return m_player; }

    explicit operator bool() = delete;
    constexpr Player opponent() const {
        switch (m_player) {
        case First:
            return Player(Second);
        case Second:
            return Player(First);
        case None:
            return Player(None);
        }
    }
private:
    Value m_player = First;
};

class Board;

namespace detail {
/**
 * @brief A backgammon field consists of a number of single fields each used exclusively by one player.
 */
class Field {
private:
public:
    std::vector<int> m_tokens;
    int m_placeable[2]{};
    friend class Board;
    template<int>
    friend class FieldIterator;

public:
    Field();
    Field(const std::vector<int>& initial);
    inline size_t size() const { return m_tokens.size();}

    inline int& toPlace(Player p) {
        return m_placeable[p == Player::Second];
    }

    inline int toPlace(Player p) const {
        return m_placeable[p == Player::Second];
    }
}; //class Field

}

class FieldState {
    const int m_state = 0;
    const Player m_player = Player::None;
public:
    constexpr FieldState(int state, Player player)
        : m_state(state), m_player(player) {}
    FieldState() = default;
    constexpr Player player() const {
        if (m_state == 0)
            return Player::None;
        else if ((m_state < 0) ^ (m_player == Player::Second))
            return Player::Second;
        else
            return Player::First;
    }
    constexpr int tokens() const { return m_state < 0 ? -m_state : m_state; }
};

namespace detail {
template <int Direction = 1>
class FieldIterator {
    static_assert (Direction == 1 || Direction == -1, "Direction must be either 1 or -1");
private:
    const Field& m_field;
    size_t m_pos = 0;
    Player m_player = Player::None;
    FieldIterator(const Field& field, const Player& p)
        : m_field(field), m_pos(), m_player(p) {}
    friend class backgammon::Board;
public:
    FieldState operator*() const {
        if ((m_player == Player::Second) ^ (Direction < 0))
            return {m_field.m_tokens[m_field.size() - m_pos - 1], m_player};
        else
            return {m_field.m_tokens[m_pos], m_player};
    }

    FieldIterator& operator +=(int diff) {
        m_pos += diff;
        return *this;
    }

    FieldIterator operator +(int diff) const {
        return FieldIterator(*this) += (Direction * diff);
    }

    FieldIterator& operator -=(int diff) {
        return operator+=(-Direction * diff);
    }

    FieldIterator operator -(int diff) const {
        return operator+(-Direction * diff);
    }

    FieldIterator& operator++() {
        return operator+=(1);
    }

    FieldIterator operator++(int) {
        auto old = FieldIterator(*this);
        operator ++();
        return old;
    }

    FieldIterator& operator--() {
        return operator-=(1);
    }

    FieldIterator operator--(int) {
        auto old = FieldIterator(*this);
        operator --();
        return old;
    }

    bool operator <(const FieldIterator& other) const {
        return m_pos < other.m_pos;
    }

    bool operator <=(const FieldIterator& other) const {
        return m_pos <= other.m_pos;
    }
    bool operator >(const FieldIterator& other) const {
        return !(*this <= other);
    }
    bool operator >=(const FieldIterator& other) const {
        return !(*this < other);
    }
    bool operator ==(const FieldIterator& other) const {
        return m_pos == other.m_pos;
    }
    bool operator !=(const FieldIterator& other) const {
        return !(*this == other);
    }
};

}

/**
 * @brief The FieldView presents the board as seen by a player.
 */
class Board {
    //TODO use shared pointer instead
    detail::Field m_field = {};
    Player m_player = {};

    Board(const detail::Field& f, const Player& player = Player::First);

    void move(size_t from, size_t steps);
    void insert(size_t at);
public:
    Board(const std::vector<int>& initial);
    Board() = default;
    using iterator = detail::FieldIterator<1>;
    using reverse_iterator = detail::FieldIterator<-1>;

    inline size_t size() const { return m_field.size(); }

    FieldState operator[](size_t index) const;;
    iterator begin() const;;
    iterator end() const;;
    reverse_iterator rbegin() const;
    reverse_iterator rend() const;

    Board opponentView() const;
    Board byPlayer(Player p) const;
    int placeable() const;

    Board moved(size_t from, size_t steps) const;
    Board inserted(size_t at) const;

    bool canMove(size_t from, size_t to) const;
    bool canInsert(size_t at) const;
}; // class FieldView

class Move {
    size_t m_from;
    size_t m_step;
public:
    Move(size_t from, size_t step);

    size_t from() const;
    size_t step() const;
};

class Insert {
    size_t m_at;
public:
    Insert(size_t at);

    size_t at() const;
};

using Action = std::variant<Move, Insert>;

using PlayerFunction = std::function<std::vector<Action>(const Board&, const std::vector<int>&)>;

class Game {
    struct DiceCounter {
        int operator()(const Move& m);
        int operator()(const Insert& i);
        int operator()(const Action& a);
    } usedDie;

    struct Applier {
        Board b;
        Board operator()(const Move& m);
        Board operator()(const Insert& i);
        Board operator()(Board board, const std::vector<Action>& actions);
    } applier;

    Board board;
    PlayerFunction player1;
    PlayerFunction player2;
    unsigned seed;

    void operator() (const Move& m);

    void operator() (const Insert& m);

    bool check(const std::vector<Action>& actions, std::vector<int> dice);

    void apply(const std::vector<Action>& actions);

    Game(PlayerFunction p1, PlayerFunction p2, unsigned seed)
        :player1(p1), player2(p2), seed(seed) {}
    void run();
public:
    static void runGame(PlayerFunction p1, PlayerFunction p2);
};
} // namespace backgammon

template<>
struct std::iterator_traits<backgammon::FieldState> {
    using difference_type = int;
    using value_type = backgammon::FieldState;
    using reference = backgammon::FieldState&;
    using pointer = backgammon::FieldState*;
    //using iterator_category;
};

#endif // BACKGAMMON_HPP
