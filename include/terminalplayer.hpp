#ifndef GAME_HPP
#define GAME_HPP

#include <backgammon.hpp>
#include <iostream>
#include <iomanip>

namespace backgammon {

namespace detail {

std::string token(Player p);

void printGame(const Board& field);

auto terminalPlayerIntermediate(const Board& b, const std::vector<int>& dice, std::vector<Action>& actions) -> std::pair<std::vector<int>, Board>;

void terminalPlayerStep(const Board& b, const std::vector<int>& dice, std::vector<Action>& actions);

}; // namespace detail

std::vector<Action> terminalPlayer(const Board& b, const std::vector<int>& dice);

}; //namespace backgammon
#endif // GAME_HPP
